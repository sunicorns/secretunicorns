organization := "org.bitbucket.sunicorns"
organizationName := "Secret Unicorns"
organizationHomepage := Some(url("https://bitbucket.org/sunicorns"))
name := "secretunicorns-spark"
description := "Secretunicorns Spark provides a unicorns implementation using Spark interface."
homepage := Some(url("https://bitbucket.org/sunicorns"))
scmInfo := Some(
  ScmInfo(
    url("https://bitbucket.org/sunicorns/secretunicorns-spark"),
    "https://sunicorns@bitbucket.org/sunicorns/secretunicorns-spark.git"
  )
)
licenses := Seq("Apache License, Version 2.0" -> url("https://www.apache.org/licenses/LICENSE-2.0.txt"))

scalaVersion := "2.11.7"

// to change the version of spark add -DSPARK_VERSION=2.x.x when running sbt
// for example: "sbt -DSPARK_VERSION=2.1.1 clean compile test doc package"
val sparkVersion = System.getProperty("SPARK_VERSION", "2.2.0")
version := "spark_" + sparkVersion + "-2.0"

lazy val secretunicornsSpark = (project in file("."))
  .configs(IntegrationTest)
  .settings(
    Defaults.itSettings: _*)

libraryDependencies ++= Seq(
  "org.apache.hadoop" % "hadoop-aws" % "2.8.1",
  "com.amazonaws" % "aws-java-sdk-s3" % "1.11.185",
  "com.amazonaws" % "aws-java-sdk-sts" % "1.11.185",
  "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-mllib" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
  "org.scalatest" %% "scalatest" % "3.0.4" % Test,
  "org.scalatest" %% "scalatest" % "3.0.4" % IntegrationTest,
  "com.amazonaws" % "aws-java-sdk-emr" % "1.11.185" % IntegrationTest,
  "org.mockito" % "mockito-all" % "1.10.19" % "test"
)


// add a task to print the classpath. Also use the packaged JAR instead
// of the .class files.
exportJars := true
lazy val printClasspath = taskKey[Unit]("Dump classpath")
printClasspath := (fullClasspath in Runtime value) foreach { e => println(e.data) }

// make scalastyle gate the build
(compile in Compile) := {
  scalastyle.in(Compile).toTask("").value
  (compile in Compile).value
}

// make the Integration Tests depend on packaging first.
(test in IntegrationTest) := {
  (Keys.`package` in Compile).value
  (Keys.`package` in IntegrationTest).value
  (test in IntegrationTest).value
}
(testOnly in IntegrationTest) := {
  (Keys.`package` in Compile).value
  (Keys.`package` in IntegrationTest).value
  (testOnly in IntegrationTest).evaluated
}

testOptions in IntegrationTest += Tests.Setup(() => {
  System.setProperty("SDK_JAR", (artifactPath in (Compile, packageBin)).value.getPath)
  System.setProperty("IT_TEST_JAR", (artifactPath in (IntegrationTest, packageBin)).value.getPath)
  System.setProperty("SPARK_VERSION", sparkVersion)
})

// we want to run integ tests in parallel and unit tests sequentially
parallelExecution in IntegrationTest := true
parallelExecution in Test := false

// set how many tests can be run at same time
val numTestsRunInParallel = 20
concurrentRestrictions := Tags.limitAll(numTestsRunInParallel) :: Nil

// publishing configuration
publishMavenStyle := true
pomIncludeRepository := { _ => false }
publishArtifact in Test := false
publishTo := {
  val nexus = "https://oss.sonatype.org/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases"  at nexus + "service/local/staging/deploy/maven2")
}
pomExtra := (
  <developers>
    <developer>
      <id>sunicorns</id>
      <organization>Secret Unicorns Org</organization>
      <organizationUrl>https://bitbucket.org/sunicorns</organizationUrl>
      <roles>
        <role>developer</role>
      </roles>
    </developer>
  </developers>
  )