class HelloWorld:
    def __init__(self):
        self.helloWorld = "Hello world!!"

    def hello(self):
        print "......."
        print self.helloWorld
        print "......."

    def helloFromScala(self):
        print "--------"
        print
        print "--------"
